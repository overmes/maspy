class UserParser:

    @staticmethod
    def get_user_name(grab):
        username_regex = grab.xpath("//a[re:match(text(), '<title>(.*?)\'s(.*)</title>')]")
        if username_regex:
            username = username_regex
        else:
            username = None

        return username

    @staticmethod
    def get_list(grab, type, time_flag):
        result_dict = {}

        error_list = grab.doc.tree.xpath('//myanimelist/error/text()')
        error = error_list[0] if error_list else None

        if not error:
            user_id = int(grab.doc.tree.xpath('//myanimelist/myinfo/user_id/text()')[0])

            xml_list = grab.doc.tree.xpath('//' + type)
            dict_len = len(xml_list)
            scores_len = 0
            for xml_node in xml_list:
                id = int(xml_node.xpath('series_' + type + 'db_id/text()')[0])
                id = -id if type == 'manga' else id
                # because mongo support only string dict keys
                id = str(id)
                status = int(xml_node.xpath('my_status/text()')[0])
                score = int(xml_node.xpath('my_score/text()')[0])
                scores_len = scores_len + 1 if score else scores_len
                time = int(xml_node.xpath('my_last_updated/text()')[0])
                if time_flag:
                    result_dict[id] = [score, status, time]
                else:
                    result_dict[id] = [score, status]
            return user_id, result_dict, dict_len, scores_len, error
        else:
            return None, None, None, None, error