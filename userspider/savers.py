class AbstractSaver:
    def __init__(self):
        self.name = 'Saver'

    def save_result(self, user_data):
        print user_data

    def in_memory(self, user_id=None, username=None):
        print self.name, 'check in memory'
        return False


class MongoSaver(AbstractSaver):

    def __init__(self, db):
        AbstractSaver.__init__(self)
        self.db = db
        self.users_col = self.db.users

    def save_result(self, user_data):
        if self.users_col:
            self.users_col.save({'_id': user_data.uid, 'username': user_data.username,
                                 'len': user_data.score_len(),
                                 'scores': user_data.score_union(), 'error': user_data.error})

    def in_memory(self, user_id=None, username=None):
        if user_id:
            if self.users_col.find({'_id': user_id}).count():
                return True
            else:
                return False
        elif username:
            if self.users_col.find({'username': username}).count():
                return True
            else:
                return False