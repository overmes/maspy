# coding=utf-8
class UserData:
    """
    класс для хранения данных пользователя
    имя, ид, списки аниме и манги, ошибки
    """

    def __init__(self, uid, username, error):
        self.uid = uid
        self.username = username
        self.error = error
        self.anime_dict = None
        self.manga_dict = None
        self.anime_score_len = 0
        self.manga_score_len = 0

    def score_union(self):
        res = {}
        res.update(self.anime_dict) if self.anime_dict else None
        res.update(self.manga_dict) if self.manga_dict else None
        return res

    def score_len(self):
        res = 0
        res += self.anime_score_len if self.anime_score_len else 0
        res += self.manga_score_len if self.manga_score_len else 0
        return res

    def set_anime(self, anime_dict, all_len, score_len):
        self.anime_dict = anime_dict
        self.anime_len = all_len
        self.anime_score_len = score_len

    def set_manga(self, manga_dict, all_len, score_len):
        self.manga_dict = manga_dict
        self.manga_len = all_len
        self.manga_score_len = score_len

    def __str__(self):
        return '{0} \t {1} \t {2} \t {3} \t {4}'.format(self.uid, self.username, self.anime_dict, self.manga_dict,
                                                        self.error)
