# coding=utf-8
from grab import Grab
from grab.spider import Spider, Task
from weblib.logs import default_logging

from generators import IdTaskGenerator, OneUserTaskGenerator
from savers import AbstractSaver
from parser import UserParser
from collections import OrderedDict

default_logging(grab_log='grab.log', network_log='grab.network.log')


class UserSpider(Spider):
    main_url = 'http://myanimelist.net'
    profile_by_id = '/profile.php?id={0}'
    main_api_url = '/malappinfo.php?u={0}&status={1}&type={2}'
    cookies_try = 1
    delay = 0

    saver = AbstractSaver()
    generator = IdTaskGenerator()
    headers = OrderedDict([
        ('Host', 'myanimelist.net'),
        ('Connection', 'keep-alive'),
        ('Cache-Control', 'max-age=0'),
        ('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'),
        ('User-Agent', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko)'
                       ' Ubuntu Chromium/36.0.1985.125 Chrome/36.0.1985.125 Safari/537.36'),
        ('Accept-Encoding', 'gzip,deflate,sdch'),
        ('Accept-Language', 'en-US,en;q=0.8,ru;q=0.6'),
    ])

    def __init__(self, generator=None, saver=None, check_memory=False, cookies_count=20, *args, **kwargs):
        self.cookies_count = cookies_count
        create_cookie_file_if_not_exist(cookies_count)
        super(UserSpider, self).__init__(*args, **kwargs)
        if generator:
            self.generator = generator
        if saver:
            self.saver = saver
        self.check_memory = check_memory
        self.grab_config.update({'transport': 'maspy.libs.curl.CurlTransport'})

    def task_generator(self):
        for user_id, username in self.generator.get_task():
            if self.check_memory and self.saver.in_memory(user_id=user_id, username=username):
                continue
            user_data = UserData(user_id, username, None)
            if username:
                url = self.main_url + (self.main_api_url.format(username, 'all', 'anime'))
                grab = Grab(url=url, reuse_cookies=True,
                            cookiefile=get_cookies_file(cookies_count=self.cookies_count),
                            headers=self.headers)
                yield Task('get_user_anime_list', user_data=user_data, grab=grab)
            elif user_id:
                url = self.main_url + (self.profile_by_id.format(user_id))
                grab = Grab(url=url, reuse_cookies=True,
                            cookiefile=get_cookies_file(cookies_count=self.cookies_count),
                            headers=self.headers)
                yield Task('get_user_name', user_data=user_data, grab=grab)

    def task_get_user_name(self, grab, task):
        print task.url
        head_meta_name = grab.pyquery.find('head meta').attr('name')
        if not head_meta_name or str(head_meta_name).lower() == 'robots':
            grab.config['cookiefile'] = get_cookies_file(grab.config['cookiefile'], cookies_count=self.cookies_count)
            yield Task('get_user_name', task_try_count=task.task_try_count + 1, user_data=task.user_data, grab=grab)
        else:
            h1_block = grab.doc.tree.xpath('//h1/text()')
            print h1_block, head_meta_name
            if h1_block and h1_block[0] != 'Invalid user':
                username = h1_block[0].split("'")[0]
                print username
                task.user_data.username = username
                url = self.main_url + (self.main_api_url.format(username, 'all', 'anime'))
                grab = Grab(url=url, cookies={}, reuse_cookies=True,
                            cookiefile=get_cookies_file(cookies_count=self.cookies_count),
                            headers=self.headers)
                yield Task('get_user_anime_list', user_data=task.user_data, grab=grab)
            else:
                task.user_data.error = h1_block
                self.saver.save_result(task.user_data)

    def task_get_user_name_fallback(self, task):
        task.user_data.error = 'Cant take username'
        self.saver.save_result(task.user_data)

    def task_get_user_anime_list(self, grab, task):
        print task.url
        head_meta_name = grab.pyquery.find('head meta').attr('name')
        if str(head_meta_name).lower() == 'robots':
            grab.config['cookiefile'] = get_cookies_file(grab.config['cookiefile'], cookies_count=self.cookies_count)
            yield Task('get_user_anime_list', task_try_count=task.task_try_count + 1, user_data=task.user_data, grab=grab)
        else:
            user_id, result_dict, dict_len, scores_len, error = UserParser.get_list(grab, 'anime', True)
            if not error:
                task.user_data.uid = user_id
                task.user_data.set_anime(result_dict, dict_len, scores_len)

                url = self.main_url + (self.main_api_url.format(task.user_data.username, 'all', 'manga'))
                grab = Grab(url=url, cookies={}, reuse_cookies=True,
                            cookiefile=get_cookies_file(cookies_count=self.cookies_count),
                            headers=self.headers)
                yield Task('get_user_manga_list', user_data=task.user_data, grab=grab)
            else:
                task.user_data.error = error
                self.saver.save_result(task.user_data)

    def task_get_user_anime_list_fallback(self, task):
        task.user_data.error = 'Cant take scores'
        self.saver.save_result(task.user_data)

    def task_get_user_manga_list(self, grab, task):
        print task.url
        head_meta_name = grab.pyquery.find('head meta').attr('name')
        if str(head_meta_name).lower() == 'robots':
            grab.config['cookiefile'] = get_cookies_file(grab.config['cookiefile'], cookies_count=self.cookies_count)
            yield Task('get_user_manga_list', task_try_count=task.task_try_count + 1, user_data=task.user_data, grab=grab)
        else:
            user_id, result_dict, dict_len, scores_len, error = UserParser.get_list(grab, 'manga', True)
            task.user_data.set_manga(result_dict, dict_len, scores_len)
            task.user_data.error = error
            self.saver.save_result(task.user_data)

    def task_get_user_manga_list_fallback(self, task):
        task.user_data.error = 'Cant take scores'
        self.saver.save_result(task.user_data)

if __name__ == '__main__':
    saver = AbstractSaver()
    id_generator = IdTaskGenerator(user_id_range=[1, 100])
    bot = UserSpider(generator=id_generator, thread_number=1, task_try_limit=20, network_try_limit=50,
                     check_memory=True, cookies_count=10)
    bot.run()

    user_generator = OneUserTaskGenerator(username='overmes')
    bot = UserSpider(generator=user_generator, thread_number=1, task_try_limit=5, network_try_limit=50,
                     check_memory=False, cookies_count=1)
    bot.run()