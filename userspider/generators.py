class AbstractGenerator:
    def __init__(self):
        self.name = 'Generator'

    def get_task(self):
        print self.name
        return [None, None]


class IdTaskGenerator(AbstractGenerator):
        def __init__(self, user_id_range=None):
            AbstractGenerator.__init__(self)
            if not user_id_range:
                user_id_range = [0, 0]
            self.min_user_id = user_id_range[0]
            self.max_user_id = user_id_range[1]

        def get_task(self):
            for user_id in xrange(self.min_user_id, self.max_user_id):
                yield (user_id, None)


class OneUserTaskGenerator(AbstractGenerator):
    def __init__(self, username=None):
        AbstractGenerator.__init__(self)
        self.username = username

    def get_task(self):
        yield None, self.username