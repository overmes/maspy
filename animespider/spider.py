# coding=utf-8
from collections import OrderedDict
import pprint
from grab import Grab
from grab.spider import Spider, Task
from weblib.logs import default_logging

from anime_parser import AnimeParser
from animespider.manga_parser import MangaParser
from libs.cookie import create_cookie_file_if_not_exist, get_cookies_file

default_logging(grab_log='grab.log', network_log='grab.network.log')


class AnimeSpider(Spider):
    headers = OrderedDict([
        ('Host', 'myanimelist.net'),
        ('Connection', 'keep-alive'),
        ('Cache-Control', 'max-age=0'),
        ('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'),
        ('User-Agent', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko)'
                       ' Ubuntu Chromium/36.0.1985.125 Chrome/36.0.1985.125 Safari/537.36'),
        ('Accept-Encoding', 'gzip,deflate,sdch'),
        ('Accept-Language', 'en-US,en;q=0.8,ru;q=0.6'),
    ])

    def __init__(self, db, type, cookies_count=10, *args, **kwargs):
        self.cookies_count = cookies_count
        create_cookie_file_if_not_exist(cookies_count=cookies_count)
        super(AnimeSpider, self).__init__(*args, **kwargs)
        self.db = db
        self.type = type
        self.anime = db.anime
        self.invalid = db.invalid
        self.grab_config.update({'transport': 'libs.curl.CurlTransport'})

        if type == 'anime':
            self.url = 'http://myanimelist.net/anime/%s'
            self.parser = AnimeParser
        elif type == 'manga':
            self.url = 'http://myanimelist.net/manga/%s'
            self.parser = MangaParser

    def in_memory(self, anime_id):
        if self.check_memory and \
                (self.invalid.find({'_id': {'t': self.type, 'i': anime_id}}).count()):
            return True
        else:
            return False

    def task_generator(self):
        for anime_id in xrange(self.MIN_ANIME_ID, self.MAX_ANIME_ID):
            if not self.in_memory(anime_id):
                url = self.url % anime_id
                g = Grab(url=url, reuse_cookies=True, headers=self.headers,
                         cookiefile=get_cookies_file(cookies_count=self.cookies_count))

                yield Task('download', grab=g, anime_id=anime_id)

    def task_download(self, grab, task):
        print task.url
        head_meta_name = grab.pyquery.find('head meta').attr('name')
        if not head_meta_name or str(head_meta_name).lower() == 'robots':
            grab.config['cookiefile'] = get_cookies_file(grab.config['cookiefile'], cookies_count=self.cookies_count)
            yield Task('download', grab=grab, task_try_count=task.task_try_count + 1, anime_id=task.anime_id)
        else:
            h1_block = grab.doc.tree.xpath('//h1/text()')
            print h1_block, head_meta_name
            if h1_block and h1_block[0] != 'Invalid Request':
                result = self.parser.get_all_fields_dict(grab)
                doc_id = {'t': result['type'], 'i': task.anime_id}
                del result['type']
                pprint.pprint(result)
                if self.anime.find({'_id': doc_id}).count():
                    self.anime.update({'_id': doc_id}, {'$set': result}, manipulate=False)
                else:
                    result['_id'] = doc_id
                    self.anime.save(result)
            else:
                self.invalid.save({'_id': {'t': self.type, 'i': task.anime_id}, 'h1': h1_block})

    @staticmethod
    def task_download_fallback(task):
        print 'DownloadError: %s' % task.url

if __name__ == '__main__':
    import pymongo
    con = pymongo.MongoClient()
    db = con.anime_test
    spider = AnimeSpider
    bot = AnimeSpider(db, 'anime', thread_number=1, task_try_limit=20, network_try_limit=50)
    bot.MIN_ANIME_ID = 22199
    bot.MAX_ANIME_ID = 22200
    bot.check_memory = False

    bot.run()
