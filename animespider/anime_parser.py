import calendar
from collections import defaultdict
from datetime import datetime
from dateutil.parser import parse


def no_except(fn):
    def wrapped(*args, **kwargs):
        try:
            return fn(*args, **kwargs)
        except BaseException as ex:
            print 'ParseError: %s %s' % (fn.func_name, ex.message)
            return None
    return wrapped


class AnimeParser:
    def __init__(self):
        pass

    @staticmethod
    def get_all_fields_dict(grab):
        title = AnimeParser.get_title(grab)
        type = AnimeParser.get_type(grab)
        image = AnimeParser.get_image(grab)
        episodes = AnimeParser.get_episodes(grab)
        status = AnimeParser.get_status(grab)
        rating = AnimeParser.get_rating(grab)
        members_score = AnimeParser.get_score(grab)
        duration = AnimeParser.get_duration(grab)
        synopsis = AnimeParser.get_synopsis(grab)
        genres = AnimeParser.get_genres(grab)
        members = AnimeParser.get_members(grab)
        scored = AnimeParser.get_scored(grab)
        english = AnimeParser.get_english(grab)
        japanese = AnimeParser.get_japanese(grab)
        synonyms = AnimeParser.get_synonyms(grab)
        producers = AnimeParser.get_producers(grab)
        favorites = AnimeParser.get_favorites(grab)

        aired_from, aired_to = AnimeParser.get_aired_from_to(grab)

        related = AnimeParser.get_related(grab)
        return {'title': title, 'type': type, 'episodes': episodes, 'status': status, 'genres': genres,
                'rating': rating, 'members_score': members_score, 'related': related,
                'aired_from': aired_from, 'aired_to': aired_to, 'duration': duration, 'synopsis': synopsis,
                'image': image, 'members': members, 'english': english, 'japanese': japanese, 'synonyms': synonyms,
                'scores': scored, 'producers': producers, 'favorites': favorites}

    @staticmethod
    @no_except
    def get_aired_from_to(grab):
        aired_from_to = AnimeParser.get_aired(grab)
        aired_split = aired_from_to.split('to')
        default_date = datetime.today().replace(month=1, day=1)
        aired_from = calendar.timegm(parse(aired_split[0].strip(), fuzzy=True, default=default_date).utctimetuple())
        if len(aired_split) > 1:
            if aired_split[1].strip() == '?':
                aired_to = None
            else:
                aired_to = calendar.timegm(parse(aired_split[1].strip(), fuzzy=True, default=default_date).utctimetuple())
        else:
            aired_to = aired_from
        return aired_from, aired_to

    @staticmethod
    @no_except
    def parse_duration_time(time_str):
        hr = 0
        hr_split = time_str.split('hr.')
        if len(hr_split) > 1:
            hr = int(hr_split[0].strip())
            time_str = hr_split[1]

        min_split = time_str.split('min.')[0].strip()
        minutes = int(min_split) if min_split else 0
        return hr*60 + minutes

    @staticmethod
    @no_except
    def get_related(grab):
        result = defaultdict(list)
        for tr in grab.doc.pyquery('.anime_detail_related_anime tr').items():
            related = tr('td:first').text().lower()[:-1]
            for link in tr('a').items():
                related_url = link.attr['href']
                related_type, str_id = related_url.split('/')[1:3]
                result[related].append({'t': related_type, 'i': int(str_id)})
        return result

    @staticmethod
    @no_except
    def get_title(grab):
        return grab.doc.tree.xpath('//h1/span/text()')[0].strip()

    @staticmethod
    @no_except
    def get_type(grab):
        return grab.doc.tree.xpath('//*[@id="content"]/table/tr/td[1]//span[text()="Type:"]/../a/text()')[0]

    @staticmethod
    @no_except
    def get_image(grab):
        return grab.doc.tree.xpath('//*[@id="content"]/table/tr/td[1]/div[1]//img')[0].attrib['data-src']

    @staticmethod
    @no_except
    def get_episodes(grab):
        episodes = grab.doc.tree.xpath('//*[@id="content"]/table/tr/td[1]//span[text()="Episodes:"]')[0].tail.strip()
        if episodes and episodes != 'Unknown':
            episodes = int(episodes)
        else:
            episodes = None
        return episodes

    @staticmethod
    @no_except
    def get_status(grab):
        return grab.doc.tree.xpath('//*[@id="content"]/table/tr/td[1]//span[text()="Status:"]')[0].tail.strip()

    @staticmethod
    @no_except
    def get_rating(grab):
        return grab.doc.tree.xpath('//*[@id="content"]/table/tr/td[1]//span[text()="Rating:"]')[0].tail.strip()

    @staticmethod
    @no_except
    def get_english(grab):
        xpath_result = grab.doc.tree.xpath('//*[@id="content"]/table/tr/td[1]//span[text()="English:"]')
        if xpath_result:
            return xpath_result[0].tail.strip()

    @staticmethod
    @no_except
    def get_synonyms(grab):
        xpath_result = grab.doc.tree.xpath('//*[@id="content"]/table/tr/td[1]//span[text()="Synonyms:"]')
        if xpath_result:
            return xpath_result[0].tail.strip()

    @staticmethod
    @no_except
    def get_japanese(grab):
        xpath_result = grab.doc.tree.xpath('//*[@id="content"]/table/tr/td[1]//span[text()="Japanese:"]')
        if xpath_result:
            return xpath_result[0].tail.strip()

    @staticmethod
    @no_except
    def get_members(grab):
        text = grab.doc.tree.xpath('//*[@id="content"]/table/tr/td[1]//span[text()="Members:"]')[0].tail
        return int(text.replace(',', '').strip())

    @staticmethod
    @no_except
    def get_favorites(grab):
        text = grab.doc.tree.xpath('//*[@id="content"]/table/tr/td[1]//span[text()="Favorites:"]')[0].tail
        return int(text.replace(',', '').strip())

    @staticmethod
    @no_except
    def get_scored(grab):
        members = grab.doc.pyquery('[itemprop="ratingCount"]').text().replace(',', '')
        if not members:
            scored_str = grab.doc.tree.xpath('//*[@id="content"]/table/tr/td[1]//span[text()="Score:"]/../small')
            members = scored_str[0].text.split()[-2] if scored_str else ''

        if not members:
            span = grab.doc.tree.xpath('//*[@id="content"]/table/tr/td[1]//span[text()="Score:"]/../span[3]/text()')
            members = span[0] if span else ''
        return int(members)

    @staticmethod
    @no_except
    def get_score(grab):
        score = grab.doc.pyquery('[itemprop="ratingValue"]').text()
        if not score:
            score_str = grab.doc.tree.xpath('//*[@id="content"]/table/tr/td[1]//span[text()="Score:"]/../text()')
            score = score_str[0].strip() if score_str else ''
        if not score:
            span = grab.doc.tree.xpath('//*[@id="content"]/table/tr/td[1]//span[text()="Score:"]/../span[2]/text()')
            score = span[0] if span else ''
        return float(score)

    @staticmethod
    @no_except
    def get_duration(grab):
        duration_str = grab.doc.tree.xpath('//*[@id="content"]/table/tr/td[1]//span[text()="Duration:"]')[0].tail.strip()
        if duration_str != 'Unknown':
            duration = AnimeParser.parse_duration_time(duration_str)
        else:
            duration = None
        return duration

    @staticmethod
    @no_except
    def get_aired(grab):
        return grab.doc.tree.xpath('//*[@id="content"]/table/tr/td[1]//span[text()="Aired:"]')[0].tail.strip()

    @staticmethod
    @no_except
    def get_synopsis(grab):
        description = grab.doc.pyquery('[itemprop="description"]').text()
        description = description or grab.pyquery('h2:contains("Synopsis")').parent().text()[14:]
        return description

    @staticmethod
    @no_except
    def get_genres(grab):
        return grab.doc.tree.xpath('//*[@id="content"]/table/tr/td[1]//span[text()="Genres:"]/../a/text()')

    @staticmethod
    @no_except
    def get_producers(grab):
        return grab.doc.tree.xpath('//*[@id="content"]/table/tr/td[1]//span[text()="Producers:"]/../a/text()')
