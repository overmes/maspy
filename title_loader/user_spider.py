# coding=utf-8
from grab import Grab
from grab.spider import Spider, Task
from weblib.logs import default_logging

from userspider.generators import IdTaskGenerator, OneUserTaskGenerator
from userspider.parser import UserParser
from userspider.savers import AbstractSaver
from userspider.user_data import UserData

default_logging(grab_log='grab.log', network_log='grab.network.log')


class UserSpider(Spider):
    main_url = 'http://myanimelist.net'
    profile_by_id = '/comments.php?id={0}'
    main_api_url = '/malappinfo.php?u={0}&status={1}&type={2}'

    saver = AbstractSaver()
    generator = IdTaskGenerator()

    def __init__(self, generator=None, saver=None, check_memory=False, *args, **kwargs):
        super(UserSpider, self).__init__(*args, **kwargs)
        if generator:
            self.generator = generator
        if saver:
            self.saver = saver
        self.check_memory = check_memory

    def task_generator(self):
        for user_id, username in self.generator.get_task():
            if self.check_memory and self.saver.in_memory(user_id=user_id, username=username):
                continue
            user_data = UserData(user_id, username, None)
            if username:
                url = self.main_url + (self.main_api_url.format(username, 'all', 'anime'))
                grab = Grab(url=url, reuse_cookies=True)
                yield Task('get_user_anime_list', user_data=user_data, grab=grab)
            elif user_id:
                url = self.main_url + (self.profile_by_id.format(user_id))
                grab = Grab(url=url)
                yield Task('get_user_name', user_data=user_data, grab=grab)

    def task_get_user_name(self, grab, task):
        h1_block = grab.doc.tree.xpath('//h1/text()')
        if grab.response.code != 404:
            username = h1_block[0].strip().split("'")[0]
            task.user_data.username = username
            url = self.main_url + (self.main_api_url.format(username, 'all', 'anime'))
            grab = Grab(url=url)
            yield Task('get_user_anime_list', user_data=task.user_data, grab=grab)

    def task_get_user_name_fallback(self, task):
        task.user_data.error = 'Cant take username'
        self.saver.save_result(task.user_data)

    def task_get_user_anime_list(self, grab, task):
        user_id, result_dict, dict_len, scores_len, error = UserParser.get_list(grab, 'anime', True)
        if not error:
            task.user_data.uid = user_id
            task.user_data.set_anime(result_dict, dict_len, scores_len)

            url = self.main_url + (self.main_api_url.format(task.user_data.username, 'all', 'manga'))
            grab = Grab(url=url)
            yield Task('get_user_manga_list', user_data=task.user_data, grab=grab)
        else:
            task.user_data.error = error
            self.saver.save_result(task.user_data)

    def task_get_user_anime_list_fallback(self, task):
        task.user_data.error = 'Cant take scores'
        self.saver.save_result(task.user_data)

    def task_get_user_manga_list(self, grab, task):
        user_id, result_dict, dict_len, scores_len, error = UserParser.get_list(grab, 'manga', True)
        task.user_data.set_manga(result_dict, dict_len, scores_len)
        task.user_data.error = error
        self.saver.save_result(task.user_data)

    def task_get_user_manga_list_fallback(self, task):
        task.user_data.error = 'Cant take scores'
        self.saver.save_result(task.user_data)

if __name__ == '__main__':
    saver = AbstractSaver()
    user_generator = OneUserTaskGenerator(username='Baman')
    bot = UserSpider(generator=user_generator, thread_number=1, task_try_limit=5, network_try_limit=10,
                     check_memory=False, saver=saver)
    bot.run()

    id_generator = IdTaskGenerator(user_id_range=[1, 100])
    bot = UserSpider(generator=id_generator, thread_number=1, task_try_limit=20, network_try_limit=50,
                     check_memory=True, saver=saver)
    bot.run()

    user_generator = OneUserTaskGenerator(username='overmes')
    bot = UserSpider(generator=user_generator, thread_number=1, task_try_limit=5, network_try_limit=50,
                     check_memory=False, saver=saver)
    bot.run()
