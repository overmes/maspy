# coding=utf-8
import pprint
from grab import Grab
from grab.spider import Spider, Task
from weblib.logs import default_logging

from animespider.anime_parser import AnimeParser
from animespider.manga_parser import MangaParser

default_logging(grab_log='grab.log', network_log='grab.network.log')


class AnimeSpider(Spider):

    def __init__(self, db, type, cookies_count=10, *args, **kwargs):
        super(AnimeSpider, self).__init__(*args, **kwargs)
        self.db = db
        self.type = type
        self.anime = db.anime
        self.invalid = db.invalid

        if type == 'anime':
            self.url = 'http://myanimelist.net/anime/%s'
            self.parser = AnimeParser
        elif type == 'manga':
            self.url = 'http://myanimelist.net/manga/%s'
            self.parser = MangaParser

    def in_memory(self, anime_id):
        title_id = {'t': self.type, 'i': anime_id}
        if self.check_memory and self.invalid.find({'_id': title_id}).count():
            return True
        else:
            return False

    def task_generator(self):
        for anime_id in xrange(self.MIN_ANIME_ID, self.MAX_ANIME_ID):
            if not self.in_memory(anime_id):
                url = self.url % anime_id
                g = Grab(url=url)

                yield Task('download', grab=g, anime_id=anime_id)

    def task_download(self, grab, task):
        if grab.response.code != 404:
            result = self.parser.get_all_fields_dict(grab)
            doc_id = {'t': result['type'], 'i': task.anime_id}
            del result['type']
            pprint.pprint(doc_id)
            if self.anime.find({'_id': doc_id}).count():
                self.anime.update({'_id': doc_id}, {'$set': result}, manipulate=False)
            else:
                result['_id'] = doc_id
                self.anime.save(result)

    @staticmethod
    def task_download_fallback(task):
        print 'DownloadError: %s' % task.url

if __name__ == '__main__':
    import pymongo
    con = pymongo.MongoClient()
    db = con.mal
    spider = AnimeSpider
    bot = AnimeSpider(db, 'anime', thread_number=1, task_try_limit=10, network_try_limit=10)
    bot.MIN_ANIME_ID = 5114
    bot.MAX_ANIME_ID = 5115
    bot.check_memory = False

    bot.run()
