package main

import (
	"fmt"
	"gopkg.in/mgo.v2"
)

type Person struct {
	Name  string
	Phone string
}

func check(err error) {
	if err != nil {
		panic(err.Error())
	}
}

func main() {
	session, err := mgo.Dial("127.0.0.1")
	if err != nil {
		panic(err)
	}
	defer session.Close()
	db := session.DB("mal")

	file, err := db.GridFS("fs").Create("test.txt")
	check(err)
	n, err := file.Write([]byte("Hello world!"))
	check(err)
	err = file.Close()
	check(err)
	fmt.Printf("%d bytes written\n", n)

}
