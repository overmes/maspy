package parser

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"io"
	"log"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
)

const MALMainUrl string = "http://myanimelist.net"
const MALCharactersUrl string = "/anime/%v/rand/characters"
const MALCharacterDetailUrl string = "/character/%v/rand/pictures"

type Title struct {
	Id         bson.M `bson:"_id"`
	Group      int    `bson:"group"`
	Characters []int  `bson:"characters"`
}

type Character struct {
	Id        int      `bson:"_id"`
	Group     []int    `bson:"group"`
	Name      string   `bson:"name"`
	Main      []int    `bson:"main"`
	Support   []int    `bson:"support"`
	Favorites int      `bson:"favor"`
	Images    []string `bson:"images"`
}

type CharacterSlice []Character

func (c CharacterSlice) GetIds() []int {
	result := make([]int, len(c))
	for i, v := range c {
		result[i] = v.Id
	}
	return result
}

func (c CharacterSlice) Len() int {
	return len(c)
}

func (c CharacterSlice) Swap(i, j int) {
	c[i], c[j] = c[j], c[i]
}

func (c CharacterSlice) Less(i, j int) bool {
	return c[i].Favorites < c[j].Favorites
}

func DownloadCharacterImage(imageUrl, dstPath string) {
	out, err := os.Create(dstPath)
	defer out.Close()
	if err != nil {
		panic(err)
	}

	resp, err := http.Get(imageUrl)
	defer resp.Body.Close()
	if err != nil {
		panic(err)
	}

	_, err = io.Copy(out, resp.Body)
	if err != nil {
		panic(err)
	}
}

func GetCharactersData(animeId, GroupId int) CharacterSlice {
	titleUrl := fmt.Sprintf(MALMainUrl+MALCharactersUrl, animeId)
	res, err := http.Get(titleUrl)
	if err != nil {
		panic(err)
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		panic("Status code not 200")
	}
	doc, err := goquery.NewDocumentFromResponse(res)
	if err != nil {
		panic(err)
	}

	result := make(CharacterSlice, 0)
	doc.Find(".js-scrollfix-bottom-rel tr").Each(func(i int, s *goquery.Selection) {
		fileUrl, charName, charType, charUrl := "", "", "", ""
		s.Find(`td > div .picSurround > a[href^="/character"]`).Each(func(i int, s *goquery.Selection) {
			fileUrl = s.Find("img").AttrOr("data-src", "")
		})
		if fileUrl != "" {
			charInfo := s.Find(`td > a[href^="/character"]`)
			charName = charInfo.Text()
			charUrl = charInfo.AttrOr("href", "")
			charType = s.Find(`td > div > small`).Text()
		}

		if fileUrl != "" && !strings.Contains(fileUrl, "questionmark") {
			charId, err := strconv.Atoi(strings.Split(charUrl, "/")[2])
			if err != nil {
				panic(err)
			}
			character := Character{charId, []int{GroupId}, charName, make([]int, 0), make([]int, 0), 0, nil}
			if charType == "Main" {
				character.Main = []int{animeId}
			} else {
				character.Support = []int{animeId}
			}
			result = append(result, character)
		}
	})

	return result
}

func GetCharacterImagesAndFavorites(char *Character) {
	characterUrl := fmt.Sprintf(MALMainUrl+MALCharacterDetailUrl, char.Id)
	res, err := http.Get(characterUrl)
	if err != nil {
		panic(err)
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		panic("Status code not 200")
	}
	doc, err := goquery.NewDocumentFromResponse(res)
	if err != nil {
		panic(err)
	}

	var favorRegexp = regexp.MustCompile(`Member Favorites: ([\d]*)`)
	favorFullString := favorRegexp.FindString(doc.Text())
	favorString := strings.TrimSpace(strings.Split(favorFullString, ":")[1])
	favorites, err := strconv.Atoi(favorString)
	if err != nil {
		panic(err)
	}
	char.Favorites = favorites

	result := make([]string, 0)
	mainImage := doc.Find(`[style="text-align: center;"] img`).AttrOr("src", "")
	if mainImage != "" {
		result = append(result, mainImage)
	}

	doc.Find(`[width="225"][align="center"] img`).Each(func(i int, s *goquery.Selection) {
		imageUrl := s.AttrOr("src", "")

		if imageUrl != "" && imageUrl != mainImage {
			result = append(result, imageUrl)
		}
	})
	char.Images = result
}

func UpdateCharacter(char Character) {
	updateData := bson.M{"group": char.Group[0]}
	if len(char.Main) > 0 {
		updateData["main"] = char.Main[0]
	} else {
		updateData["support"] = char.Support[0]
	}
	err := charCollection.UpdateId(char.Id, bson.M{"$addToSet": updateData})
	if err != nil {
		panic(err)
	}
}

func UpdateCharactersData(animeId, groupId int) {

	characters := GetCharactersData(animeId, groupId)
	fmt.Println("Characters:")
	for i := range characters {
		existed, err := charCollection.FindId(characters[i].Id).Count()
		if err != nil {
			panic(err)
		}
		if existed == 0 {
			GetCharacterImagesAndFavorites(&characters[i])
			_, err := charCollection.UpsertId(characters[i].Id, characters[i])
			if err != nil {
				panic(err)
			}
			fmt.Println(" id ", characters[i].Id, " images ", len(characters[i].Images))
		} else {
			UpdateCharacter(characters[i])
			fmt.Println(" id ", characters[i].Id, " updated ")
		}
	}

	titleQuery := bson.M{"_id.i": animeId, "_id.t": bson.M{"$in": animeTypes}}
	err := animeCollection.Update(titleQuery, bson.M{"$set": bson.M{"characters": characters.GetIds()}})
	if err != nil {
		panic(err)
	}
}

func GetTopAnimeTitles(limit int) []Title {
	var result []Title

	err := animeCollection.Find(bson.M{"_id.t": bson.M{"$in": animeTypes},
		"scores": bson.M{"$gte": 1000}}).Sort("-members_score").Limit(limit).All(&result)
	if err != nil {
		log.Fatal(err)
	}
	return result
}

var animeCollection *mgo.Collection
var charCollection *mgo.Collection
var animeTypes = []string{"TV", "Movie", "OVA", "Special", "ONA", "Music"}

func StartParser(limit int) {
	session, err := mgo.Dial("127.0.0.1")
	if err != nil {
		panic(err)
	}
	defer session.Close()
	animeCollection = session.DB("mal").C("anime")
	charCollection = session.DB("mal").C("char")

	topAnime := GetTopAnimeTitles(limit)

	for _, title := range topAnime {

		titleId := 0
		switch title.Id["i"].(type) {
		case int:
			titleId = title.Id["i"].(int)
		case float64:
			titleId = int(title.Id["i"].(float64))
		}
		UpdateCharactersData(titleId, title.Group)

		//if len(title.Characters) == 0 {
		//	UpdateCharactersData(titleId, title.Group)
		//} else {
		//	fmt.Println("skip ", title.Id)
		//}
	}
}
