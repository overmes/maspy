import argparse
import pymongo

from title_loader.anime_spider import AnimeSpider
from title_loader.user_spider import UserSpider
from userspider.generators import IdTaskGenerator
from userspider.savers import MongoSaver

arguments = argparse.ArgumentParser()
arguments.add_argument('--type', dest='type', type=str, help='spider type anime/manga/users')
arguments.add_argument('-d', '--db', dest='db', type=str, default='test', help='mongodb for saving results')
arguments.add_argument('--from', dest='id_from', type=int, default=1, help='load from this id')
arguments.add_argument('--to', dest='id_to', type=int, default=100, help='load to this id')
arguments.add_argument('--threads', dest='threads', type=int, default=1, help='threads count')
arguments.add_argument('-m', '--memory', dest='memory', const=True, default=False, help='check memory',
                       action='store_const')

args = arguments.parse_args()

con = pymongo.MongoClient()
db = con[args.db]

if args.type == 'anime' or args.type == 'manga':
    spider = AnimeSpider
    bot = AnimeSpider(db, args.type, thread_number=args.threads, task_try_limit=10, network_try_limit=10)
    bot.MIN_ANIME_ID = args.id_from
    bot.MAX_ANIME_ID = args.id_to
    bot.check_memory = args.memory
    bot.run()

elif args.type == 'users':
    saver = MongoSaver(db)
    id_generator = IdTaskGenerator(user_id_range=[args.id_from, args.id_to])
    bot = UserSpider(generator=id_generator, thread_number=args.threads, task_try_limit=10, network_try_limit=10,
                     check_memory=args.memory, saver=saver)
    bot.run()
