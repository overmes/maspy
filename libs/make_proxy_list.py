temp_file = open('temp.txt').read()
proxy_rows = temp_file.split('\n')
res = []
for proxy_row in proxy_rows[::2]:
    ip = proxy_row.split('\t')[1].strip()
    port = proxy_row.split('\t')[2].strip()
    res.append('%s:%s' % (ip, port))

proxy_file = open('proxy.txt', 'w')
proxy_file.write('\n'.join(res))


