import os
import random


def create_cookie_file_if_not_exist(cookies_count):
    for i in range(0, cookies_count):
        if not os.path.exists("cookies"+str(i)):
            f = file("cookies"+str(i), "w")
            f.write("{}")


def delete_cookies(cookie_file):
    print 'Remove cookie ' + cookie_file
    with open(cookie_file, "w") as f:
        f.write('{}')


def get_cookies_file(cookie_file=None, state={}, cookies_count=10):
    if cookie_file:
        count = state.setdefault(cookie_file, 0)
        if count > 3:
            delete_cookies(cookie_file)
            state[cookie_file] = 0
        state[cookie_file] += 1
    rnd = random.randint(0, cookies_count-1)
    return 'cookies' + str(rnd)